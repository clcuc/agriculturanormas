.. Agricultura-Law documentation master file, created by
   sphinx-quickstart on Thu Jul  2 13:22:20 2020.

Normas legales sobre Agricultura
================================

.. toctree::
   :maxdepth: 2
   :caption: Leyes

   de Transparencia             <leyes/ds-021-2019-jus>

.. toctree::
   :maxdepth: 2
   :caption: Decretos Supremos

   Manejo Residuos Solidos          <decsup/ds-016-2012-ag>
   Reg Participacion Ciudadana      <decsup/ds-018-2012-ag>
   Gestion Ambiental Sec Agrario    <decsup/ds-019-2012-ag>
   Calidad Ambiental Suelo          <decsup/ds-011-2017-minam>
   Gestion Sitios Contaminados      <decsup/ds-012-2017-minam>

.. toctree::
   :maxdepth: 2
   :caption: Resoluciones Ministeriales

   Suspension de Plazos (2020)      <ministerial/rm-0131-2020-minagri>
   Descontaminacion de Suelos       <ministerial/rm-085-2014-minam>

.. toctree::
   :maxdepth: 2
   :caption: Resoluciones Directorales

   RDG 007-2020-Minagri-DVDIAR-DGAAA     <dgaaa/rdg_007-2020-minagri-dvdiar-dgaaa_ocr>

.. 
   Índices y tablas
   ================

   * :ref:`genindex`
   * :ref:`modindex`

* :ref:`search`
